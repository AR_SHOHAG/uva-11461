#include <cstdio>
#include <algorithm>
#include <cmath>
using namespace std;

int main()
{
    long int a, b, i, t, ck=0;

    while(scanf("%ld%ld", &a,&b), a||b){
        if(a>b)
            swap(a,b);
        for(i=a;i<=b;++i){
            t=sqrt(i);
            if(t*t==i)
                ck++;
        }
        printf("%ld\n", ck);
        ck=0;
    }
    return 0;
}
